<!doctype html>

<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        
        <title>Prueba Intelsa</title>
        
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="<?php echo e(asset('css/app.css')); ?>" />
    </head>
    <body>
        <div id="app"></app>

        <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    </body>
</html><?php /**PATH /opt/lampp/htdocs/prueba_intelsa/resources/views/app.blade.php ENDPATH**/ ?>