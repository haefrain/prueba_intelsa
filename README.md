<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Prueba Intelsa - Efrain Hernandez

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/haefrain/prueba_intelsa/-/tree/master/)

Se realiza la prueba utilizando el framework Laravel en su version 7.0 y vue 2.5.

Este proyecto no es 100% una SPA, es un hibrido, donde se utilizaron vistas de laravel con componentes de Vue.

# Caracteristicas

  - Registro de usuarios
  - Iniciar sesion
  - Registro de Estudiantes
  - Edicion de Estudiantes
  - Listado de Estudiantes
  - Borrado suave de Estudiantes


### Instalacion

Prueba Intelsa requiere [Node.js](https://nodejs.org/), [Composer](https://getcomposer.org/download/) y un servidor que ejecute php y una base de datos MySQL, el utilizado durante el desarrollo fue [XAMPP](https://www.apachefriends.org/es/download.html)

Recuerde que este proyecto se debe ejecutar en un servidor que ejecute php, en el caso de XAMPP los archivos deben ser colocados en la carpeta htdocs.

Una vez todo lo necesario este instalado se debe proceder a crear una base de datos vacia con el nombre a gusto o en su defecto ejecutar el siguiente comando en la consola de MySQL o el cliente de base de datos de preferencia.

```sh
CREATE DATABASE prueba_intelsa;
```

Una vez creada la base de datos debes instalar las dependencias y las dependencias del entorno de desarrollo en caso de requerir que el proyecto corra en su entorno de desarrollo

```sh
$ cd prueba_intelsa
$ npm install -d
$ npm run watch
```
y tambien
```sh
$ composer install
```
una vez todos los paquetes esten instalados procederemos a configurar los datos de acceso del motor de base de datos para esto configuraremos el archivo .env, en este caso copiaremos el archivo .env.example y le colocaremos el nombre .env

en linux (Ubuntu) puede utilizar el siguiente comando
```sh
$ cp .env.example .env
```

Modificar las credenciales de la base de datos en el archivo .env, mas especificamente las siguientes keys
```sh
$ DB_HOST=
$ DB_PORT=
$ DB_DATABASE=
$ DB_USERNAME=
$ DB_PASSWORD=
```
una vez configurada, abra en el navegador la url de su servidor (por defecto localhost si lo esta ejecutando de forma local).