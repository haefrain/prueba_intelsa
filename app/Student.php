<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $table = 'students';
    protected $fillable = ['university', 'type_document', 'document_number', 'first_name', 'last_name', 'sex', 'birth_date', 'career'];
    public $timestamps = true;
}
