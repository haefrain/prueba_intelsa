<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use App\Student;

class StudentController extends Controller
{

    public function index()
    {
        $students = Student::get();
        return response()->json($students);
    }

    public function store(StudentRequest $request)
    {        
        $data['university'] = $request->university;
        $data['type_document'] = $request->type_document;
        $data['document_number'] = $request->document_number;
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['sex'] = $request->sex;
        $data['birth_date'] = $request->birth_date;
        $data['career'] = $request->career;

        $result = Student::insertGetId($data);
        if ($result) {
            $response = ['success' => true, 'id' => $result];
        } else {
            $response = ['success' => false];
        }
        
        
        return response()->json($response);
    }

    public function update(StudentRequest $request, $id)
    {
        $data['university'] = $request->university;
        $data['type_document'] = $request->type_document;
        $data['document_number'] = $request->document_number;
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['sex'] = $request->sex;
        $data['birth_date'] = $request->birth_date;
        $data['career'] = $request->career;

        $result = Student::find($id)->update($data);

        $response = ['success' => $result];

        return response()->json($response);
    }

    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();
        return response()->json(['success' => true]);
    }
}
