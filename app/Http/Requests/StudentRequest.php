<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'university' => 'required',
            'type_document' => 'required',
            'document_number' => 'required|max:30',
            'first_name' => 'required|max:150',
            'last_name' => 'required|max:150',
            'sex' => 'required',
            'birth_date' => 'required|date',
            'career' => 'required|max:150'

        ];
    }
}
