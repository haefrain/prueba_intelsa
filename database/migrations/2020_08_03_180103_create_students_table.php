<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('university', 150);
            $table->enum('type_document', ['Cedula de Ciudadania', 'Tarjeta de identidad', 'Pasaporte', 'Cedula de extranjeria'])->comment('Type document of student is enum');
            $table->string('document_number', 30)->unique()->comment('Document number of student is very important, search student by field');
            $table->string('first_name', 150)->comment('first name of student is string');
            $table->string('last_name', 150)->comment('last name of student is string');
            $table->enum('sex', ['masculino', 'femenino'])->comment('Sex of student');
            $table->date('birth_date')->comment('Birth date of student');
            $table->string('career', 150)->comment('Career of student');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
